# Abstract
This repo contains a simple Java Spring microservice with a pipeline deploying it on an existing k8s cluster

# Prerequisites
- A service account with "Kubernetes Engine Admin" and “Storage Admin” roles.
- GKE, GCR(and container scanner) APIs enabled
- GKE 1.14+
- Istio 1.4.5+ - service mesh, for the advanced networking/proxying
- Flagger 0.23.0+ - that tool is doing the heavy-lifting of canaries, creatign Istio VS and changing weights on a gateway under the hood
- Helm 3.1.1+ - templating tool used for installations
- Gitlab CI with the following environmental variables set up:
    - PROJECT -  your GCP project
    - CREDENTIALS - copy and paste a key retrieved during the creation of the service account
    - CLUSTER_NAME - your kubernetes cluster name
    - ZONE - zone where your kubernetes cluster is deployed
    - SONAR_HOST_URL - sonarqube server URL
    - SONAR_TOKEN - sonarqube server token

##### Also, branches should be named within the DNS naming convention, because a deployment is exposed under a `<branch_name>.<app_name>.<domain_name>` pattern.

# How the pipeline works
Pipeline is controlled by `.gitlab-ci.yml` file and consists of the following stages:
- unit_test - Unit test against source will be run

- sonar - sonarqube static analysis

- build_jar - A jar artifact will be build and passed down the pipeline

- build_docker_image - A docker image will be build and passed down the pipeline

- smoke_test_docker_image - The docker image will be spinned up and tested if it can be run

- upload_docker_image - The docker image will be uploaded to a private GCR, then checked for critical security issues and deleted and abort if any CRITICAL issues were found

- deploy_to_kubernetes - Deployment part can take two different ways : dev and prod. It depends on the branch the pipeline is currently being executed against.
    - In case of the branch being called not "staging" or "master" we assume that it's for the development/feature testing purposes thus can be run with lower resources, on a non-prod cluster and doesn't necessarily need canary deployments.If there's no previous deployment of that branch in that repo helm will make a clean install including 1 Deployment, 1 HPA, 1 SVC, 1 Istio VS, 1 Istio GW. If that branch was already deployed helm will run upgrade which will effectively affect only one parameter from the whole suite - docker image of the deployment. After a successful upgrade deployment will undergo a RollingUpdate and shortly will serve an updated application version

    - In case of the branch being called either "staging" or "master" we assume that we are doing real-world deployment and canary deployments come in place. Again, depending on existence of previous deployments of that branch helm is going to do either clean install or upgrade. Installation includes 1 Deployment, 1 HPA, 1 Istio GW and 1 Flagger Canary.
If it's a clean installation flagger will do a regular deployment. If it's an upgrade flagger will run a canary deployment with 20% step until reaches 60%, at that point both primary and canary release will be switched to a new image. During the deployment flagger will constantly check on a `request-success-rate` metric and if it drops below 99 percentile in 1m interval deployment will be aborted and rolled-back

# Further actions
After a successful deployment the app is reachable via `<branch_name>.<app_name>.<domain_name>`, e.g `master.hello-world.acme.com`.
