variables:
  PROJECT: $PROJECT
  IMAGE_REGISTRY: gcr.io/$PROJECT/$CI_PROJECT_NAME
  CREDENTIALS: /tmp/gcloud_credentials.json
  CLUSTER_NAME: $CLUSTER_NAME
  ZONE: $ZONE
  NAMESPACE: $CI_COMMIT_REF_NAME-$CI_PROJECT_NAME

stages:
  - unit_test
  - sonar
  - build_jar
  - build_docker_image
  - smoke_test_docker_image
  - upload_docker_image
  - deploy_to_kubernetes

unit_test:
  image: gradle:jdk8
  stage: unit_test
  script:
    - gradle --info test
  artifacts:
    paths:
      - build/reports/tests/*
    when: always
    expire_in: 2 weeks
  except:
    - staging
    - master

sonar:
  image: gradle:jdk8
  stage: sonar
  script: gradle sonarqube
  except:
    - staging
    - master

build_jar:
  image: gradle:jdk8
  stage: build_jar
  script:
    - gradle --info bootJar
  artifacts:
    paths:
      - build/libs/*.jar
    expire_in: 2 weeks
  except:
    - staging
    - master

build_docker_image:
  image: docker:stable
  services:
    - docker:dind
  stage: build_docker_image
  script:
    - docker build -t $IMAGE_REGISTRY:$CI_COMMIT_SHA  . 
    - docker save $IMAGE_REGISTRY:$CI_COMMIT_SHA > image.tar
  artifacts:
    paths:
      - image.tar
    expire_in: 2 weeks
  except:
    - staging
    - master

smoke_test_docker_image:
  image: docker:stable
  services:
    - name: docker:dind
      alias: localhost
  stage: smoke_test_docker_image
  script:
    - apk add curl
    - docker load -i image.tar
    - docker run -d -p 80:8080 $IMAGE_REGISTRY:$CI_COMMIT_SHA
    - sleep 60
    - curl -v localhost
  except:
    - staging
    - master

upload_docker_image:
  image: docker:stable
  stage: upload_docker_image
  services:
    - name: docker:dind
      alias: localhost
  script:
    - apk add curl bash python jq
    - curl -L https://sdk.cloud.google.com | bash
    - export PATH="$PATH:/root/google-cloud-sdk/bin"
    - gcloud components install -q beta
    - echo $SERVICE_ACCOUNT > $CREDENTIALS
    - gcloud auth activate-service-account --key-file $CREDENTIALS
    - gcloud auth -q configure-docker
    - gcloud config set project $PROJECT
    - docker load -i image.tar
    - docker push $IMAGE_REGISTRY:$CI_COMMIT_SHA
    - |
      critical_vulnerabilities_count=$(gcloud beta container images describe $IMAGE_REGISTRY:$CI_COMMIT_SHA --format json  --show-all-metadata | \
      jq  '.package_vulnerability_summary.vulnerabilities.CRITICAL | length')
    - |
      if [[ $critical_vulnerabilities_count -gt 0 ]]; then
        echo "$critical_vulnerabilities_count critical vulnerabilities were found, aborting now" && sleep 30 && yes | gcloud beta container images delete $IMAGE_REGISTRY:$CI_COMMIT_SHA && exit 1
      fi
  except:
    - staging
    - master

deploy_to_kubernetes:
  image: google/cloud-sdk:alpine
  stage: deploy_to_kubernetes
  script:
    - apk add openssl
    - curl -fsSL https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
    - echo $SERVICE_ACCOUNT > $CREDENTIALS
    - gcloud auth activate-service-account --key-file $CREDENTIALS
    - gcloud container clusters get-credentials $CLUSTER_NAME --zone $ZONE --project $PROJECT
    - gcloud components install kubectl
    - kubectl create namespace $NAMESPACE || echo
    - kubectl label namespace $NAMESPACE istio-injection=enabled || echo
    - cd infra/k8s/
    - ./tools/helm_deploy.sh
    - ./tools/flagger_status.sh
    - echo "You can check your deployment out at ${CI_COMMIT_REF_NAME}.${CI_PROJECT_NAME}.acme.com"
