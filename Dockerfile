FROM openjdk:8-jdk-alpine
COPY build/libs/hello-world.jar hello-world.jar
ENTRYPOINT ["java", "-jar", "/hello-world.jar"]
