#!/bin/bash


if [[ "$CI_COMMIT_REF_NAME" == "master" ]] || [[ "$CI_COMMIT_REF_NAME" == "staging" ]]; then
  PROGRESSING=false
  INGRESS="$(kubectl -n istio-system get svc istio-ingressgateway --output jsonpath="{.status.loadBalancer.ingress[0].ip}")"
  DEPLOYMENT_REVISION="$(helm ls | grep -w "$CI_COMMIT_REF_NAME"-"$CI_PROJECT_NAME" | awk '{print $3}')"
else
  exit 0
fi


if  [[ "$DEPLOYMENT_REVISION" == 1 ]]; then
  echo "Initial deployment was successful"
  exit 0
fi


while true; do

  curl -sSH "Host:$CI_COMMIT_REF_NAME.$CI_PROJECT_NAME.acme.com" "$INGRESS"

  STATUS="$(kubectl -n "$CI_COMMIT_REF_NAME"-"$CI_PROJECT_NAME" get canary "$CI_PROJECT_NAME" \
        --output jsonpath="{.status.phase}")"

  echo "Status: $STATUS"

  if [[ "$PROGRESSING" == "false" && "$STATUS" == "Progressing" ]]; then
      PROGRESSING=true
  elif [[ "$PROGRESSING" == "true" && "$STATUS" == "Succeeded" ]]; then
    echo "Canary deployment succeeded"
    break
  elif [[ "$PROGRESSING" == "true" && "$STATUS" == "Failed" ]]; then
    echo "Canary deployment failed"
    EXIT_CODE=1
    break
  fi

  sleep 1

done


exit $EXIT_CODE
