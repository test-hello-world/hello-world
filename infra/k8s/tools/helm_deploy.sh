#!/bin/bash


if [[ "$CI_COMMIT_REF_NAME" == "master" ]] || [[ "$CI_COMMIT_REF_NAME" == "staging" ]]; then
  ENV="prod"
  CI_COMMIT_SHA="$(git show -s HEAD^2 --pretty=%H)"
else
  ENV="dev"
fi


if [[ "$ENV" == "prod" ]]; then
  helm upgrade -i "$CI_COMMIT_REF_NAME"-"$CI_PROJECT_NAME" ./"$ENV" --set appname="$CI_PROJECT_NAME" --set image="$IMAGE_REGISTRY":"$CI_COMMIT_SHA" --set sha="$CI_COMMIT_SHA" --set branch="$CI_COMMIT_REF_NAME"
else
  helm upgrade -i "$CI_COMMIT_REF_NAME"-"$CI_PROJECT_NAME" ./"$ENV" --set appname="$CI_PROJECT_NAME" --set image="$IMAGE_REGISTRY":"$CI_COMMIT_SHA" --set sha="$CI_COMMIT_SHA" --set branch="$CI_COMMIT_REF_NAME"
fi


if [[ "$(helm ls | grep  "$CI_COMMIT_REF_NAME"-"$CI_PROJECT_NAME" | awk '{print $8}')" != "deployed" ]]; then
  echo "Deployment failed"
  exit 1
fi
